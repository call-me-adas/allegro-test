// FUNCTIONS
function loadChannels(url) {
    return fetch(url)
        .then(res => res.json())
        .catch(err => { throw console.log(err) });
}

function generateDOM(array) {
    const container = document.getElementById("container");
    let template = '';
    array.forEach((item) => {
        template += `<div class="container__item">
                        <a href="${item.customUrl}" target="_blank"><img class="container__image" src="${item.thumbnails.default.url}" /></a>
                        <h2 class="container__title">${item.title}</h2>
                        <div class="container__headers">
                            <div class="subHeader">
                                <span class="subHeader__description">Subscribers</span>
                                <span class="subHeader__value">${new Intl.NumberFormat('en-GB').format(item.statistics.subscriberCount)}</span>
                            </div>
                            <div class="subHeader">
                                <span class="subHeader__description">Videos</span>
                                <span class="subHeader__value">${new Intl.NumberFormat('en-GB').format(item.statistics.videoCount)}</span>
                            </div>
                            <div class="subHeader">
                                <span class="subHeader__description">Views</span>
                                <span class="subHeader__value">${new Intl.NumberFormat('en-GB').format(item.statistics.viewCount)}</span>
                            </div>
                        </div>
                    </div>`;
    });
    container.innerHTML  = template ;
}

function cleanContainer() {
    document.getElementById("container").innerHTML = '';
}

function sortChannels(name, array) {
    return array.sort((a, b) => {
        if(name.split('.').length === 1) {
            return a[name] !== b[name] ? a[name] < b[name] ? -1 : 1 : 0;
        } else {
            const names = name.split('.');
            const a1 = parseInt(a[names[0]][names[1]]);
            const b1 = parseInt(b[names[0]][names[1]]);
            return a1 !== b1 ? a1 < b1 ? -1 : 1 : 0;
        }
    });
}

function filterChannels(value, array) {
    const filtered = array.filter(x => x.title.toLowerCase().includes(value.toLowerCase()));
    return value === '' ? array : filtered;
}

function generatedChangedDOM(array) {
    cleanContainer();
    generateDOM(array);
}

function handleEvents(channels) {
    let tempChannels = [...channels];
    let sortName = '';
    console.log(channels);

    // SORT
    const radioTitle = document.getElementById("sort-title");
    const radioSubscribers = document.getElementById("sort-subscribers");
    const radioVideos = document.getElementById("sort-videos");
    const radioViews = document.getElementById("sort-views");

    function sortBy(name) {
        sortName = name;
        tempChannels = sortChannels(name, tempChannels);
        generatedChangedDOM(tempChannels);
    }

    radioTitle.addEventListener('click', () => sortBy('title'));
    radioSubscribers.addEventListener('click',() => sortBy('statistics.subscriberCount'));
    radioVideos.addEventListener('click', () => sortBy('statistics.videoCount'));
    radioViews.addEventListener('click', () => sortBy('statistics.viewCount'));

    // FILTER
    const filter = document.getElementById("filter");

    filter.addEventListener('keyup', ($event) => {
        tempChannels = filterChannels($event.target.value, channels);
        if(sortName !== '') {
            tempChannels = sortChannels(sortName, tempChannels);
        }
        generatedChangedDOM(tempChannels);
    })

    // RESET
    document.getElementById("clear").addEventListener('click', () => {
        filter.value = '';
        [radioTitle, radioSubscribers, radioVideos, radioViews].forEach((item) => item.checked = false);
        generatedChangedDOM(channels);
    });
}

/// MAIN
async function main() {
    const channels = await loadChannels(`${BASE_URL}/channels.json`);

    generateDOM(channels);

    handleEvents(channels);
}

// RUN APP
const BASE_URL = 'http://localhost:3000'
main(BASE_URL).then(() => {
    console.log('Application started');
});
